package redis

import "testing"
import "strings"

func TestBadLogin(t *testing.T) {
	be := New()
	
	user, err := be.Login("test", "test")
	if err == nil {
		t.Errorf("Expecting a user")	
	}
	if strings.Compare(err.Error(), "Bad username or password") != 0  {
		t.Errorf("Expecting %v, got %v", "Bad username or password", err)	
	}
	if user != nil {
		t.Errorf("Not expecting a user")
	}

}

func TestGoodLogin(t *testing.T) {
	be := New()
	
	user, err := be.Login("username", "password")
	if err != nil {
		t.Errorf("Not expecting any error %v", err)
	 }
	if user == nil {
	t.Errorf("Expecting a user")
	}

}